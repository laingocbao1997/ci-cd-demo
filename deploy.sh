#/bin/bash
## Find the PID of existing binary running on server
PID=$(ssh root@52.221.155.57 "ps -eaf |pgrep ci-cd-demo")
## If PID is empty, then we can just move ahead
## but if not, then kill the process
if [ "$PID" = "" ]; then
 echo "ci-cd-demo is not running on this server"
else
 ## Kill existing process
 ssh root@52.221.155.57 "kill -9 $PID"
 echo "Killing ci-cd-demo process"
fi
## Upload new binary
echo "Uploading new binary..."
scp /builds/laingocbao1997/ci-cd-demo/ci-cd-demo root@52.221.155.57:/home/demo/
echo  "done..."
## Run new binary on server
echo "Run binary..."
ssh root@52.221.155.57 "nohup /home/demo/ci-cd-demo &> /dev/null & exit"
echo "Deployment complete..."