#!/bin/bash
# Test to make sure the application is returning code 200
CODE=$(curl -s -w "%{http_code}" "http://localhost:4000/hello" -o /dev/null)
if [ $CODE == '200' ]; then
 echo "Yay! Application returned code 200"
else
 echo "Argh! Application returned error code: $CODE..."
 exit 1
fi
# Test to make sure the application is returning the string "Hello World!"
MSG=$(curl -s "http://localhost:4000/hello")
if [ "$MSG" = "Hello World!" ]; then
 echo "Yay! Application Works!"
else
 echo "No! Application Failed! Code: $MSG"
 exit 1
fi